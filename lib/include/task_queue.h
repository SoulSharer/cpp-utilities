#ifndef TASK_QUEUE_H
#define TASK_QUEUE_H

#include <functional>
#include <memory>
#include <string>
#include "utilities_export.h"

namespace Neuro::Utilities {

class TaskQueue final{
public:
	SDK_EXPORT explicit TaskQueue(const std::string &name = std::string());
    TaskQueue(const TaskQueue &) = delete;
    TaskQueue& operator=(const TaskQueue &) = delete;
	SDK_EXPORT TaskQueue(TaskQueue &&) noexcept;
	SDK_EXPORT TaskQueue& operator=(TaskQueue &&) noexcept;
	SDK_EXPORT ~TaskQueue();

	SDK_EXPORT void exec(const std::function<void()> &);

private:
	struct Impl;
	std::unique_ptr<Impl> mImpl;
};

}

#endif // TASK_QUEUE_H
