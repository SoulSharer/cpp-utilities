#ifndef MESSAGE_QUEUE_H
#define MESSAGE_QUEUE_H

#include <future>
#include <utility>
#include <list>
#include "thread_safe_queue.h"

namespace Neuro::Utilities
{
	namespace Impl
	{
		class ReceiverRevoker final
		{
		public:
			ReceiverRevoker(std::mutex& revoke_mutex,
				std::condition_variable& revoke_condition,
				std::future<void>&& revoke_future) :
				mRevokeMutex(revoke_mutex),
				mRevokeCondition(revoke_condition),
				mRevokeFuture(std::move(revoke_future))
			{}

			void onDestroy() noexcept
			{
				try
				{
					std::unique_lock revokeLock{ mRevokeMutex };
					mRevokeCondition.notify_one();
					revokeLock.unlock();
					while (mRevokeFuture.wait_for(std::chrono::milliseconds(1)) != std::future_status::ready)
					{
						std::unique_lock repeatLock{ mRevokeMutex };
						mRevokeCondition.notify_one();
					}
				}
				catch (...) {}
			}

			std::mutex& pushMutex() noexcept
			{
				return mRevokeMutex;
			}

			std::condition_variable& pushCondition() noexcept
			{
				return mRevokeCondition;
			}
			
		private:
			std::mutex& mRevokeMutex;
			std::condition_variable& mRevokeCondition;
			std::future<void> mRevokeFuture;
		};
	}	

	template <typename... Args>
	class ReceiverQueue;

	class MessageQueue;
	
	template <typename... Args>
	class MessageReceiver final
	{
	public:
		explicit MessageReceiver(std::function<void(Args ...)> receiver_func):
			mReceiverFunction(std::make_unique<std::function<void(Args ...)>>(std::move(receiver_func)))
		{}

		MessageReceiver(const MessageReceiver& other) = delete;
		MessageReceiver(MessageReceiver&& other) noexcept = delete;
		MessageReceiver& operator=(const MessageReceiver& other) = delete;
		MessageReceiver& operator=(MessageReceiver&& other) noexcept = delete;
		
		~MessageReceiver()
		{
			mReceiverFunction.reset();
			if (mRevoker != nullptr)
			{
				mRevoker->onDestroy();
			}
		}
		
	private:
		std::shared_ptr<std::function<void(Args...)>> mReceiverFunction;
		std::shared_ptr<Impl::ReceiverRevoker> mRevoker;
		
		friend class MessageQueue;
		void setRevoker(std::shared_ptr<Impl::ReceiverRevoker> revoker)
		{
			mRevoker = std::move(revoker);
		}
	
		[[nodiscard]]
		std::weak_ptr<std::function<void(Args...)>> sendFunction() const noexcept
		{
			return mReceiverFunction;
		}
	};

	template <typename... Args>
	class ReceiverQueue final
	{
	public:
		explicit ReceiverQueue(std::weak_ptr<std::function<void(Args...)>> receiver) :
			mMessageQueue(std::make_shared<ThreadSafeQueue<std::tuple<std::decay_t<Args>...>>>()),
			mReceiverFunction(std::move(receiver))
		{}

		[[nodiscard]]
		bool expired() const noexcept
		{
			return mReceiverFunction.expired();
		}

		void pushMessage(Args... args)
		{
			if (expired()) return;
			std::unique_lock pushLock{ mRevoker->pushMutex() };
			mMessageQueue->push(std::tuple<std::decay_t<Args>...>(std::move(args)...));
			mRevoker->pushCondition().notify_one();
		}

	private:
		std::shared_ptr<ThreadSafeQueue<std::tuple<std::decay_t<Args>...>>> mMessageQueue;
		std::weak_ptr<std::function<void(Args...)>> mReceiverFunction;
		std::shared_ptr<Impl::ReceiverRevoker> mRevoker;
		
		friend class MessageQueue;
		std::weak_ptr<ThreadSafeQueue<std::tuple<std::decay_t<Args>...>>> queue()
		{
			return mMessageQueue;
		}

		void setRevoker(std::shared_ptr<Impl::ReceiverRevoker> revoker)
		{
			mRevoker = std::move(revoker);
		}
	};
		
	class MessageQueue final
	{
	public:
		~MessageQueue();
		
		template <typename... Args>
		static ReceiverQueue<Args...> registerReceiver(MessageReceiver<Args...> &receiver)
		{
			ReceiverQueue<Args...> queue{ receiver.sendFunction() };			
			auto revoker = instance().pushQueue(std::make_unique<QueueImpl<Args...>>(receiver.sendFunction(), queue.queue()));
			receiver.setRevoker(revoker);
			queue.setRevoker(revoker);
			return queue;
		}

	private:
		struct BaseQueueImpl
		{
			BaseQueueImpl() = default;
			BaseQueueImpl(const BaseQueueImpl& other) = delete;
			BaseQueueImpl(BaseQueueImpl&& other) noexcept = delete;
			BaseQueueImpl& operator=(const BaseQueueImpl& other) = delete;
			BaseQueueImpl& operator=(BaseQueueImpl&& other) noexcept = delete;
			virtual ~BaseQueueImpl() = default;
			virtual void send() = 0;
			virtual bool expired() const noexcept = 0;
			virtual bool empty() const noexcept = 0;
		};

		template <typename... Args>
		struct QueueImpl : public BaseQueueImpl
		{
			QueueImpl(std::weak_ptr<std::function<void(Args...)>> function,
				std::weak_ptr<ThreadSafeQueue<std::tuple<std::decay_t<Args>...>>> queue):
				mReceiverFunction(std::move(function)),
				mMessageQueue(std::move(queue))
			{}
			
			void send() override
			{
				auto sender = mReceiverFunction.lock();
				if (sender == nullptr) return;

				auto queue = mMessageQueue.lock();
				if (queue == nullptr) return;
				
				auto message = queue->try_pop();
				if (message == nullptr) return;

				std::apply(*sender, *message);
			}

			bool expired() const noexcept override
			{
				return mReceiverFunction.expired() || mMessageQueue.expired();
			}

			bool empty() const noexcept override
			{
				auto queue = mMessageQueue.lock();
				if (queue == nullptr) return true;
				return queue->empty();
			}

		private:
			std::weak_ptr<std::function<void(Args...)>> mReceiverFunction;
			std::weak_ptr<ThreadSafeQueue<std::tuple<std::decay_t<Args>...>>> mMessageQueue;
		};

		struct QueueListElement final
		{
			QueueListElement(std::unique_ptr<BaseQueueImpl>&& queue):
				Queue(std::move(queue))
			{}
			std::unique_ptr<BaseQueueImpl> Queue;
			std::promise<void> RevokedPromise;
			std::mutex SendMutex;
			std::unique_ptr<std::future<void>> FinishedFuture;
		};
		
		std::atomic_bool mDone;
		std::mutex mRevokeMutex;
		std::condition_variable mRevokeCondition;
		std::list<QueueListElement> mListenerCollection;		
		std::promise<void> mFinishedPromise;
		std::future<void> mFinishedFuture;
		std::thread mQueueThread;

		static bool removeExpiredHasMessages(std::list<QueueListElement>& listen_queues);
		
		MessageQueue();

		static MessageQueue& instance();

		template <typename... Args>
		std::shared_ptr<Impl::ReceiverRevoker> pushQueue(std::unique_ptr<QueueImpl<Args...>>&& impl)
		{
			std::unique_lock queueLock{ mRevokeMutex };
			auto& newElement = mListenerCollection.emplace_back(std::move(impl));
			auto  revoker = std::make_shared<Impl::ReceiverRevoker>( mRevokeMutex, mRevokeCondition, newElement.RevokedPromise.get_future() );
			return revoker;
		}

		void notificationThread();
	};
}

#endif // MESSAGE_QUEUE_H
