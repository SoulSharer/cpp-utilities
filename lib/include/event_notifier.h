#ifndef EVENT_NOTIFIER_H
#define EVENT_NOTIFIER_H

#include <vector>
#include <list>
#include "event_listener.h"

namespace Neuro::Utilities
{
	template <typename... Args>
	class EventNotifier final
	{
	public:
		using EventListener = EventListener<Args...>;
		using ListenerQueue = typename EventListener::NotificationQueue;

		EventNotifier() = default;
		EventNotifier(const EventNotifier& rhs) = delete;
		EventNotifier& operator=(const EventNotifier& rhs) = delete;
		EventNotifier(EventNotifier&& rhs) = default;
		EventNotifier& operator=(EventNotifier&& rhs) = default;
		~EventNotifier() = default;

		void registerListener(const EventListener& listener)
		{
			mListenerCollection.push_back(listener.queue());
		}

		void notify(Args... args)
		{
			removeExpired(mListenerCollection);
			sendNotifications(mListenerCollection, std::move(args)...);
		}

		void notifyAsync(Args... args)
		{
			notify(std::move(args)...);
		}

	private:
		std::list<ListenerQueue> mListenerCollection;

		static void removeExpired(std::list<ListenerQueue>& listen_queues)
		{
			using EraseIterator = typename std::remove_reference_t<decltype(listen_queues)>::iterator;
			
			std::vector<EraseIterator> iteratorsToErase;
			for (auto listenerIterator = listen_queues.begin(); listenerIterator != listen_queues.end(); ++listenerIterator)
			{
				if (listenerIterator->expired()) { iteratorsToErase.push_back(listenerIterator); }
			}

			for (auto iteratorToErase : iteratorsToErase) { listen_queues.erase(iteratorToErase); }
		}

		static void sendNotifications(std::list<ListenerQueue>& listener_queues, Args... args)
		{
			for (auto& listenerQueue : listener_queues)
			{
				listenerQueue.pushMessage(args...);
			}
		}
	};
}

#endif // EVENT_NOTIFIER_H
