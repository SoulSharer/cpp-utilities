#include "thread_pool.h"

namespace Neuro::Utilities
{
	thread_local ThreadSafeQueue<ThreadPool::FunctionWrapper>* ThreadPool::LocalQueue{nullptr};
	thread_local size_t ThreadPool::LocalIndex{0};
	
	ThreadPool::ThreadPool():
		mDone(false)
	{
		const auto threadsCount = std::thread::hardware_concurrency();
		try
		{
			for (size_t i = 0; i < threadsCount; ++i)
			{
				mLocalQueues.push_back(std::make_unique<ThreadSafeQueue<FunctionWrapper>>());
			}
			for (size_t i = 0; i < threadsCount; ++i)
			{
				mThreads.push_back(std::thread(&ThreadPool::workerThread, this, i));
			}
		}
		catch (...)
		{
			mDone = true;
			throw;
		}
	}

	ThreadPool::~ThreadPool()
	{
		mDone = true;
		std::unique_lock pushLock{ mQueueMutex };
		mQueueCondition.notify_all();
		pushLock.unlock();
		for (auto& thread : mThreads)
		{
			if (thread.joinable())
			{
				thread.join();
			}
		}
	}

	ThreadPool& ThreadPool::instance()
	{
		static ThreadPool pool;
		return pool;
	}
	
	void ThreadPool::workerThread(size_t index)
	{
		LocalIndex = index;
		LocalQueue = mLocalQueues[LocalIndex].get();
		while (!mDone)
		{
			runPendingTask();
		}
	}

	void ThreadPool::runPendingTask()
	{
		FunctionWrapper task;
		if (popFromLocal(task)
			|| popFromPool(task)
			|| steal(task))
		{
			task();
		}
		else
		{
			std::unique_lock pushLock{ mQueueMutex };
			mQueueCondition.wait_for(pushLock, std::chrono::milliseconds(50));
		}
	}

	bool ThreadPool::popFromLocal(FunctionWrapper& task)
	{
		return (LocalQueue != nullptr) && LocalQueue->try_pop(task);
	}

	bool ThreadPool::popFromPool(FunctionWrapper& task)
	{
		return mPoolQueue.try_pop(task);
	}

	bool ThreadPool::steal(FunctionWrapper& task)
	{
		for (size_t i = 0; i < mLocalQueues.size(); ++i)
		{
			const auto index = (LocalIndex + i + 1) % mLocalQueues.size();
			if (mLocalQueues[index]->try_pop(task))
			{
				return true;
			}
		}

		return false;
	}
}
