#include "gtest/gtest.h"
#include "event_listener.h"

using Neuro::Utilities::EventListener;

class EventTestIntInt
{
	EventListener<int, int> mListener{[](int, int) {}};
};

class EventTestConstIntRefConstIntRef
{
	EventListener<const int &, const int &> mListener{[](const int&, const int&) {}};
};

TEST(EventListener, Constructor_LambdaNoArgs_NoException)
{
	ASSERT_NO_THROW({ auto listener = EventListener<>([](){}); });
}

TEST(EventListener, Constructor_LambdaOneIntArg_NoException)
{
	ASSERT_NO_THROW({ auto listener = EventListener<int>([](int) {}); });
}

TEST(EventListener, Constructor_LambdaOneConstIntRefArg_NoException)
{
	ASSERT_NO_THROW({ auto listener = EventListener<const int &>([](const int &) {}); });
}

TEST(EventListener, ConstructorInClass_LambdaTwoIntArgs_NoException)
{
	ASSERT_NO_THROW({ EventTestIntInt listenerClass; });
}

TEST(EventListener, ConstructorInClass_LambdaTwoConstIntRefArg_NoException)
{
	ASSERT_NO_THROW({ EventTestConstIntRefConstIntRef listenerClass; });
}

