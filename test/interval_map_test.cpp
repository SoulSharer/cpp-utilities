#include "interval_map.h"
#include "gtest/gtest.h"

using Neuro::Utilities::interval_map;

TEST(IntervalMap, IndexOperator_ValidValue)
{
	interval_map<int, char> testMap{ 'a' };
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[0] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
		&& !testMap.hasDuplicateKeys()
		&& !testMap.hasSequences()
	);

}

TEST(IntervalMap, EmptyInterval_NoChanges)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(20, -1, 'b');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[0] == 'a'
		&& testMap[20] == 'a'
		&& testMap[-1] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
		&& !testMap.hasDuplicateKeys()
		&& !testMap.hasSequences()
	);
}

TEST(IntervalMap, EmptyInterval2_NoChanges)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(20, 20, 'b');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[0] == 'a'
		&& testMap[20] == 'a'
		&& testMap[19] == 'a'
		&& testMap[21] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
		&& !testMap.hasDuplicateKeys()
		&& !testMap.hasSequences()
	);
}

TEST(IntervalMap, OneElement_ValidIntervals)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[-21] == 'a'
		&& testMap[-20] == 'b'
		&& testMap[0] == 'b'
		&& testMap[70] == 'b'
		&& testMap[119] == 'b'
		&& testMap[120] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
	);
}

TEST(IntervalMap, OneElement_NoSequencies)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	ASSERT_FALSE( testMap.hasSequences()	);
}

TEST(IntervalMap, OneElement_NoDuplicateKeys)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	ASSERT_FALSE(testMap.hasDuplicateKeys());
}

TEST(IntervalMap, SameElement_ValidIntervals)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'a');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[-21] == 'a'
		&& testMap[-20] == 'a'
		&& testMap[0] == 'a'
		&& testMap[70] == 'a'
		&& testMap[119] == 'a'
		&& testMap[120] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
	);
}

TEST(IntervalMap, SameElement_NoSequencies)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'a');
	ASSERT_FALSE
	(
		testMap.hasSequences()
	);
}

TEST(IntervalMap, SameElement_NoDuplicateKeys)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'a');
	ASSERT_FALSE
	(
		testMap.hasDuplicateKeys()
	);
}

TEST(IntervalMap, SameElementOverwrite_ValidIntervals)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(-20, 120, 'a');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[-21] == 'a'
		&& testMap[-20] == 'a'
		&& testMap[0] == 'a'
		&& testMap[70] == 'a'
		&& testMap[119] == 'a'
		&& testMap[120] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
	);
}

TEST(IntervalMap, SameElementOverwrite_NoDuplicateKeys)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(-20, 120, 'a');
	ASSERT_FALSE(testMap.hasDuplicateKeys());
}

TEST(IntervalMap, SameElementOverwrite_NoSequencies)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(-20, 120, 'a');
	ASSERT_FALSE(testMap.hasSequences());
}

TEST(IntervalMap, TwoNonOverlap_ValidIntervals)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(200, 1000, 'c');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[-21] == 'a'
		&& testMap[-20] == 'b'
		&& testMap[0] == 'b'
		&& testMap[70] == 'b'
		&& testMap[119] == 'b'
		&& testMap[120] == 'a'

		&& testMap[199] == 'a'
		&& testMap[200] == 'c'
		&& testMap[500] == 'c'
		&& testMap[700] == 'c'
		&& testMap[999] == 'c'
		&& testMap[1000] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
	);
}

TEST(IntervalMap, TwoNonOverlap_NoSequencies)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(200, 1000, 'c');
	ASSERT_FALSE
	(
		testMap.hasSequences()
	);
}

TEST(IntervalMap, TwoNonOverlap_NoDuplicateKeys)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(200, 1000, 'c');
	ASSERT_FALSE
	(
		testMap.hasDuplicateKeys()
	);
}

TEST(IntervalMap, TwoWithInclusion_ValidIntervals)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(0, 100, 'c');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[-21] == 'a'
		&& testMap[-20] == 'b'
		&& testMap[119] == 'b'
		&& testMap[120] == 'a'

		&& testMap[-1] == 'b'
		&& testMap[0] == 'c'
		&& testMap[30] == 'c'
		&& testMap[60] == 'c'
		&& testMap[99] == 'c'
		&& testMap[100] == 'b'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
	);
}

TEST(IntervalMap, TwoWithInclusion_NoSequencies)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(0, 100, 'c');
	ASSERT_FALSE
	(
		testMap.hasSequences()
	);
}

TEST(IntervalMap, TwoWithInclusion_NoDuplicateKeys)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(0, 100, 'c');
	ASSERT_FALSE
	(
		testMap.hasDuplicateKeys()
	);
}

TEST(IntervalMap, TwoWithSameValue_Canonical)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(0, 100, 'b');
	ASSERT_FALSE(testMap.hasSequences());
}

TEST(IntervalMap, TwoOverlap1_ValidIntervals)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(80, 200, 'c');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[-21] == 'a'
		&& testMap[-20] == 'b'
		&& testMap[0] == 'b'
		&& testMap[79] == 'b'

		&& testMap[80] == 'c'
		&& testMap[120] == 'c'
		&& testMap[199] == 'c'
		&& testMap[200] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
	);
}

TEST(IntervalMap, TwoOverlap1_NoSequencies)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(80, 200, 'c');
	ASSERT_FALSE
	(
		testMap.hasSequences()
	);
}

TEST(IntervalMap, TwoOverlap1_NoDuplicateKeys)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(80, 200, 'c');
	ASSERT_FALSE
	(
		testMap.hasDuplicateKeys()
	);
}

TEST(IntervalMap, TwoOverlap2_ValidIntervals)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(-100, 50, 'c');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[-101] == 'a'
		&& testMap[-100] == 'c'
		&& testMap[0] == 'c'
		&& testMap[49] == 'c'

		&& testMap[50] == 'b'
		&& testMap[80] == 'b'
		&& testMap[119] == 'b'
		&& testMap[120] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
	);
}

TEST(IntervalMap, TwoOverlap2_NoSequence)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(-100, 50, 'c');
	ASSERT_FALSE
	(		
		testMap.hasSequences()
	);
}

TEST(IntervalMap, TwoOverlap2_NoDuplicateKeys)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(-100, 50, 'c');
	ASSERT_FALSE
	(		
		testMap.hasDuplicateKeys()
	);
}

TEST(IntervalMap, TwoOverride_ValidIntervals)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(-30, 130, 'c');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[-31] == 'a'
		&& testMap[-30] == 'c'
		&& testMap[0] == 'c'
		&& testMap[70] == 'c'
		&& testMap[129] == 'c'
		&& testMap[130] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
	);
}

TEST(IntervalMap, TwoOverride_NoSequencies)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(-30, 130, 'c');
	ASSERT_FALSE
	(		
		testMap.hasSequences()
	);
}

TEST(IntervalMap, TwoOverride_NoDuplicateKeys)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(-30, 130, 'c');
	ASSERT_FALSE
	(		
		testMap.hasDuplicateKeys()
	);
}

TEST(IntervalMap, ThreeNonOverlap_ValidIntervals)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(200, 1000, 'c');
	testMap.assign(120, 200, 'd');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[-21] == 'a'
		&& testMap[-20] == 'b'
		&& testMap[0] == 'b'
		&& testMap[70] == 'b'
		&& testMap[119] == 'b'

		&& testMap[120] == 'd'
		&& testMap[150] == 'd'
		&& testMap[199] == 'd'

		&& testMap[200] == 'c'
		&& testMap[500] == 'c'
		&& testMap[700] == 'c'
		&& testMap[999] == 'c'
		&& testMap[1000] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
	);
}

TEST(IntervalMap, ThreeNonOverlap_NoSqeuencies)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(200, 1000, 'c');
	testMap.assign(120, 200, 'd');
	ASSERT_FALSE(testMap.hasSequences());
}

TEST(IntervalMap, ThreeNonOverlap_NoDuplicateKeys)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(200, 1000, 'c');
	testMap.assign(120, 200, 'd');
	ASSERT_FALSE(testMap.hasDuplicateKeys());
}

TEST(IntervalMap, ThreeOverlap1_ValidIntervals)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(200, 1000, 'c');
	testMap.assign(120, 300, 'd');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[-21] == 'a'
		&& testMap[-20] == 'b'
		&& testMap[0] == 'b'
		&& testMap[70] == 'b'
		&& testMap[119] == 'b'

		&& testMap[120] == 'd'
		&& testMap[150] == 'd'
		&& testMap[199] == 'd'
		&& testMap[200] == 'd'
		&& testMap[299] == 'd'

		&& testMap[300] == 'c'
		&& testMap[500] == 'c'
		&& testMap[700] == 'c'
		&& testMap[999] == 'c'
		&& testMap[1000] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
	);
}

TEST(IntervalMap, ThreeOverlap1_NoSequencies)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(200, 1000, 'c');
	testMap.assign(120, 300, 'd');
	ASSERT_FALSE
	(
		testMap.hasSequences()
	);
}

TEST(IntervalMap, ThreeOverlap1_NoDuplicateKeys)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 120, 'b');
	testMap.assign(200, 1000, 'c');
	testMap.assign(120, 300, 'd');
	ASSERT_FALSE
	(
		testMap.hasDuplicateKeys()
	);
}

TEST(IntervalMap, ThreeOverlap2_ValidIntervals)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 140, 'b');
	testMap.assign(120, 200, 'd');
	testMap.assign(250, 1000, 'c');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[-21] == 'a'
		&& testMap[-20] == 'b'
		&& testMap[0] == 'b'
		&& testMap[70] == 'b'
		&& testMap[119] == 'b'

		&& testMap[120] == 'd'
		&& testMap[150] == 'd'
		&& testMap[199] == 'd'

		&& testMap[200] == 'a'
		&& testMap[222] == 'a'
		&& testMap[249] == 'a'

		&& testMap[250] == 'c'
		&& testMap[500] == 'c'
		&& testMap[700] == 'c'
		&& testMap[999] == 'c'
		&& testMap[1000] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
	);
}

TEST(IntervalMap, ThreeOverlap2_NoSequencies)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 140, 'b');
	testMap.assign(120, 200, 'd');
	testMap.assign(250, 1000, 'c');
	ASSERT_FALSE
	(		
		testMap.hasSequences()
	);
}

TEST(IntervalMap, ThreeOverlap2_NoDuplicateKeys)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 140, 'b');
	testMap.assign(120, 200, 'd');
	testMap.assign(250, 1000, 'c');
	ASSERT_FALSE
	(		
		testMap.hasDuplicateKeys()
	);
}

TEST(IntervalMap, ThreeOverlap3_ValidIntervals)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 140, 'b');
	testMap.assign(150, 200, 'd');
	testMap.assign(-10, 1000, 'c');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[-21] == 'a'
		&& testMap[-20] == 'b'
		&& testMap[-11] == 'b'

		&& testMap[-10] == 'c'
		&& testMap[149] == 'c'
		&& testMap[150] == 'c'
		&& testMap[199] == 'c'
		&& testMap[200] == 'c'

		&& testMap[300] == 'c'
		&& testMap[222] == 'c'
		&& testMap[249] == 'c'

		&& testMap[250] == 'c'
		&& testMap[500] == 'c'
		&& testMap[700] == 'c'
		&& testMap[999] == 'c'
		&& testMap[1000] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
	);
}

TEST(IntervalMap, ThreeOverlap3_NoSequencies)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 140, 'b');
	testMap.assign(150, 200, 'd');
	testMap.assign(-10, 1000, 'c');
	ASSERT_FALSE
	(
		testMap.hasSequences()
	);
}

TEST(IntervalMap, ThreeOverlap3_NoDuplicateKeys)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 140, 'b');
	testMap.assign(150, 200, 'd');
	testMap.assign(-10, 1000, 'c');
	ASSERT_FALSE
	(
		testMap.hasDuplicateKeys()
	);
}

TEST(IntervalMap, FourOverlap_ValidIntervals)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 140, 'b');
	testMap.assign(150, 200, 'd');
	testMap.assign(-20, 1000, 'c');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[-21] == 'a'
		&& testMap[-20] == 'c'
		&& testMap[-11] == 'c'

		&& testMap[-10] == 'c'
		&& testMap[149] == 'c'
		&& testMap[150] == 'c'
		&& testMap[199] == 'c'
		&& testMap[200] == 'c'

		&& testMap[300] == 'c'
		&& testMap[222] == 'c'
		&& testMap[249] == 'c'

		&& testMap[250] == 'c'
		&& testMap[500] == 'c'
		&& testMap[700] == 'c'
		&& testMap[999] == 'c'
		&& testMap[1000] == 'a'
		&& testMap[11999] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
		&& !testMap.hasDuplicateKeys()
		&& !testMap.hasSequences()
	);
}

TEST(IntervalMap, FourOverlap2_ValidIntervals)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 140, 'b');
	testMap.assign(150, 200, 'd');
	testMap.assign(-20, 140, 'c');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[-21] == 'a'
		&& testMap[-20] == 'c'
		&& testMap[139] == 'c'
		&& testMap[140] == 'a'
		&& testMap[149] == 'a'
		&& testMap[150] == 'd'
		&& testMap[199] == 'd'
		&& testMap[200] == 'a'

		&& testMap[11999] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
		&& !testMap.hasDuplicateKeys()
		&& !testMap.hasSequences()
	);
}

TEST(IntervalMap, FourOverlapEnd_ValidIntervals)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 140, 'b');
	testMap.assign(150, 200, 'd');
	testMap.assign(-10, 1000, 'c');
	testMap.assign(12000, std::numeric_limits<int>::max(), 'e');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'a'
		&& testMap[-21] == 'a'
		&& testMap[-20] == 'b'
		&& testMap[-11] == 'b'

		&& testMap[-10] == 'c'
		&& testMap[149] == 'c'
		&& testMap[150] == 'c'
		&& testMap[199] == 'c'
		&& testMap[200] == 'c'

		&& testMap[300] == 'c'
		&& testMap[222] == 'c'
		&& testMap[249] == 'c'

		&& testMap[250] == 'c'
		&& testMap[500] == 'c'
		&& testMap[700] == 'c'
		&& testMap[999] == 'c'
		&& testMap[1000] == 'a'
		&& testMap[11999] == 'a'
		&& testMap[12000] == 'e'
		&& testMap[100000] == 'e'
		&& testMap[1000000] == 'e'
		&& testMap[std::numeric_limits<int>::max() - 1] == 'e'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
		&& !testMap.hasDuplicateKeys()
		&& !testMap.hasSequences()
	);
}

TEST(IntervalMap, FourOverlapBegin_ValidIntervals)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 140, 'b');
	testMap.assign(150, 200, 'd');
	testMap.assign(-10, 1000, 'c');
	testMap.assign(std::numeric_limits<int>::lowest(), -12000, 'e');
	ASSERT_TRUE
	(
		testMap[std::numeric_limits<int>::lowest()] == 'e'
		&& testMap[-12001] == 'e'
		&& testMap[-12000] == 'a'
		&& testMap[-21] == 'a'
		&& testMap[-20] == 'b'
		&& testMap[-11] == 'b'

		&& testMap[-10] == 'c'
		&& testMap[149] == 'c'
		&& testMap[150] == 'c'
		&& testMap[199] == 'c'
		&& testMap[200] == 'c'

		&& testMap[300] == 'c'
		&& testMap[222] == 'c'
		&& testMap[249] == 'c'

		&& testMap[250] == 'c'
		&& testMap[500] == 'c'
		&& testMap[700] == 'c'
		&& testMap[999] == 'c'
		&& testMap[1000] == 'a'
		&& testMap[11999] == 'a'
		&& testMap[std::numeric_limits<int>::max()] == 'a'
		&& !testMap.hasDuplicateKeys()
		&& !testMap.hasSequences()
	);
}


TEST(IntervalMap, FourOverlapEnd_Canonical)
{
	interval_map<int, char> testMap{ 'a' };
	testMap.assign(-20, 140, 'b');
	testMap.assign(150, 200, 'd');
	testMap.assign(-10, 1000, 'c');
	testMap.assign(12000, std::numeric_limits<int>::max(), 'a');
	ASSERT_TRUE
	(
		!testMap.hasSequences()
	);
}