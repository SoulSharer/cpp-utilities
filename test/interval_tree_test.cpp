#include "interval_tree.h"
#include "gtest/gtest.h"

using Neuro::Utilities::IntervalTree;
using Neuro::Utilities::Interval;

TEST(IntervalTree, Create_EmptyTree)
{
	IntervalTree<char> testMap;
	ASSERT_TRUE
	(
		testMap.empty()
	);

}

TEST(IntervalTree, AppendOne_FoundExact)
{
	IntervalTree<char> testMap;
	Interval inter{10, 20};
	char val = 'c';
	testMap.insert_or_assign(inter, val);

	auto appended = testMap.find(inter, val);
	ASSERT_NE(appended.first, testMap.end());
	ASSERT_NE(appended.first, appended.second);
	ASSERT_EQ(appended.first->Inter.Low, inter.Low);
	ASSERT_EQ(appended.first->Inter.High, inter.High);
	ASSERT_EQ(appended.first->Val, val);
	ASSERT_EQ(testMap.size(), 1);
}

TEST(IntervalTree, AppendOne_FoundIntersect1)
{
	IntervalTree<char> testMap;
	Interval inter{ 10, 20 };
	char val = 'c';
	testMap.insert_or_assign(inter, val);
	
	Interval interSearch{ 5, 10 };
	auto appended = testMap.find(interSearch, val);
	ASSERT_NE(appended.first, testMap.end());
	ASSERT_NE(appended.first, appended.second);
	ASSERT_EQ(appended.first->Inter.Low, inter.Low);
	ASSERT_EQ(appended.first->Inter.High, inter.High);
	ASSERT_EQ(appended.first->Val, val);
	ASSERT_EQ(testMap.size(), 1);
}

TEST(IntervalTree, AppendOne_FoundIntersect2)
{
	IntervalTree<char> testMap;
	Interval inter{ 10, 20 };
	char val = 'c';
	testMap.insert_or_assign(inter, val);

	Interval interSearch{ 5, 20 };
	auto appended = testMap.find(interSearch, val);
	ASSERT_NE(appended.first, testMap.end());
	ASSERT_NE(appended.first, appended.second);
	ASSERT_EQ(appended.first->Inter.Low, inter.Low);
	ASSERT_EQ(appended.first->Inter.High, inter.High);
	ASSERT_EQ(appended.first->Val, val);
	ASSERT_EQ(testMap.size(), 1);
}

TEST(IntervalTree, AppendOne_FoundIntersect3)
{
	IntervalTree<char> testMap;
	Interval inter{ 10, 20 };
	char val = 'c';
	testMap.insert_or_assign(inter, val);

	Interval interSearch{ 5, 25 };
	auto appended = testMap.find(interSearch, val);
	ASSERT_NE(appended.first, testMap.end());
	ASSERT_NE(appended.first, appended.second);
	ASSERT_EQ(appended.first->Inter.Low, inter.Low);
	ASSERT_EQ(appended.first->Inter.High, inter.High);
	ASSERT_EQ(appended.first->Val, val);
	ASSERT_EQ(testMap.size(), 1);
}

TEST(IntervalTree, AppendOne_FoundIntersect4)
{
	IntervalTree<char> testMap;
	Interval inter{ 10, 20 };
	char val = 'c';
	testMap.insert_or_assign(inter, val);

	Interval interSearch{ 10, 25 };
	auto appended = testMap.find(interSearch, val);
	ASSERT_NE(appended.first, testMap.end());
	ASSERT_NE(appended.first, appended.second);
	ASSERT_EQ(appended.first->Inter.Low, inter.Low);
	ASSERT_EQ(appended.first->Inter.High, inter.High);
	ASSERT_EQ(appended.first->Val, val);
	ASSERT_EQ(testMap.size(), 1);
}

TEST(IntervalTree, AppendOne_FoundIntersect5)
{
	IntervalTree<char> testMap;
	Interval inter{ 10, 20 };
	char val = 'c';
	testMap.insert_or_assign(inter, val);

	Interval interSearch{ 15, 20 };
	auto appended = testMap.find(interSearch, val);
	ASSERT_NE(appended.first, testMap.end());
	ASSERT_NE(appended.first, appended.second);
	ASSERT_EQ(appended.first->Inter.Low, inter.Low);
	ASSERT_EQ(appended.first->Inter.High, inter.High);
	ASSERT_EQ(appended.first->Val, val);
	ASSERT_EQ(testMap.size(), 1);
}

TEST(IntervalTree, AppendOne_FoundIntersect6)
{
	IntervalTree<char> testMap;
	Interval inter{ 10, 20 };
	char val = 'c';
	testMap.insert_or_assign(inter, val);

	Interval interSearch{ 15, 25 };
	auto appended = testMap.find(interSearch, val);
	ASSERT_NE(appended.first, testMap.end());
	ASSERT_NE(appended.first, appended.second);
	ASSERT_EQ(appended.first->Inter.Low, inter.Low);
	ASSERT_EQ(appended.first->Inter.High, inter.High);
	ASSERT_EQ(appended.first->Val, val);
	ASSERT_EQ(testMap.size(), 1);
}

TEST(IntervalTree, AppendOne_FoundIntersect7)
{
	IntervalTree<char> testMap;
	Interval inter{ 10, 20 };
	char val = 'c';
	testMap.insert_or_assign(inter, val);

	Interval interSearch{ 20, 25 };
	auto appended = testMap.find(interSearch, val);
	ASSERT_NE(appended.first, testMap.end());
	ASSERT_NE(appended.first, appended.second);
	ASSERT_EQ(appended.first->Inter.Low, inter.Low);
	ASSERT_EQ(appended.first->Inter.High, inter.High);
	ASSERT_EQ(appended.first->Val, val);
	ASSERT_EQ(testMap.size(), 1);
}

TEST(IntervalTree, AppendOne_NoIntersectNotFound1)
{
	IntervalTree<char> testMap;
	Interval inter{ 10, 20 };
	char val = 'c';
	testMap.insert_or_assign(inter, val);

	Interval interSearch{ 21, 25 };
	auto appended = testMap.find(interSearch, val);
	ASSERT_EQ(appended.first, testMap.end());
	ASSERT_EQ(appended.first, appended.second);
	ASSERT_EQ(testMap.size(), 1);
}

TEST(IntervalTree, AppendOne_NoIntersectNotFound2)
{
	IntervalTree<char> testMap;
	Interval inter{ 10, 20 };
	char val = 'c';
	testMap.insert_or_assign(inter, val);

	Interval interSearch{ 5, 9 };
	auto appended = testMap.find(interSearch, val);
	ASSERT_EQ(appended.first, testMap.end());
	ASSERT_EQ(appended.first, appended.second);
	ASSERT_EQ(testMap.size(), 1);
}

TEST(IntervalTree, AppendTwoNotIntersectsSameVal_FoundExactBoth1)
{
	IntervalTree<char> testMap;
	Interval inter1{ 10, 20 };
	Interval inter2{ 30, 40 };
	char val = 'c';
	testMap.insert_or_assign(inter1, val);
	testMap.insert_or_assign(inter2, val);

	auto appended1 = testMap.find(inter1, val);
	ASSERT_NE(appended1.first, testMap.end());
	ASSERT_NE(appended1.first, appended1.second);
	ASSERT_EQ(appended1.first->Inter.Low, inter1.Low);
	ASSERT_EQ(appended1.first->Inter.High, inter1.High);
	ASSERT_EQ(appended1.first->Val, val);

	auto appended2 = testMap.find(inter2, val);
	ASSERT_NE(appended2.first, testMap.end());
	ASSERT_NE(appended2.first, appended2.second);
	ASSERT_EQ(appended2.first->Inter.Low, inter2.Low);
	ASSERT_EQ(appended2.first->Inter.High, inter2.High);
	ASSERT_EQ(appended2.first->Val, val);
	ASSERT_EQ(testMap.size(), 2);
}

TEST(IntervalTree, AppendTwoNotIntersectsSameVal_FoundExactBoth2)
{
	IntervalTree<char> testMap;
	Interval inter1{ 45, 50 };
	Interval inter2{ 30, 40 };
	char val = 'c';
	testMap.insert_or_assign(inter1, val);
	testMap.insert_or_assign(inter2, val);

	auto appended1 = testMap.find(inter1, val);
	ASSERT_NE(appended1.first, testMap.end());
	ASSERT_NE(appended1.first, appended1.second);
	ASSERT_EQ(appended1.first->Inter.Low, inter1.Low);
	ASSERT_EQ(appended1.first->Inter.High, inter1.High);
	ASSERT_EQ(appended1.first->Val, val);

	auto appended2 = testMap.find(inter2, val);
	ASSERT_NE(appended2.first, testMap.end());
	ASSERT_NE(appended2.first, appended2.second);
	ASSERT_EQ(appended2.first->Inter.Low, inter2.Low);
	ASSERT_EQ(appended2.first->Inter.High, inter2.High);
	ASSERT_EQ(appended2.first->Val, val);
	ASSERT_EQ(testMap.size(), 2);
}

TEST(IntervalTree, AppendTwoIntersectsSameVal_FoundExactOne1)
{
	IntervalTree<char> testMap;
	Interval inter1{ 10, 20 };
	Interval inter2{ 20, 40 };
	Interval result{ 10, 40 };
	char val = 'c';
	testMap.insert_or_assign(inter1, val);
	testMap.insert_or_assign(inter2, val);


	ASSERT_EQ(testMap.size(), 1);
	auto appended = testMap.find(result, val);
	ASSERT_NE(appended.first, testMap.end());
	ASSERT_NE(appended.first, appended.second);
	ASSERT_EQ(appended.first->Inter.Low, result.Low);
	ASSERT_EQ(appended.first->Inter.High, result.High);
	ASSERT_EQ(appended.first->Val, val);
	ASSERT_EQ(++appended.first, appended.second);
}

TEST(IntervalTree, AppendTwoIntersectsSmallSameVal_FoundExactOne)
{
	IntervalTree<char> testMap;
	Interval inter1{ 1, 2 };
	Interval inter2{ 2, 3 };
	Interval result{ 1, 3 };
	char val = 'c';
	testMap.insert_or_assign(inter1, val);
	testMap.insert_or_assign(inter2, val);


	ASSERT_EQ(testMap.size(), 1);
	auto appended = testMap.find(result, val);
	ASSERT_NE(appended.first, testMap.end());
	ASSERT_NE(appended.first, appended.second);
	ASSERT_EQ(appended.first->Inter.Low, result.Low);
	ASSERT_EQ(appended.first->Inter.High, result.High);
	ASSERT_EQ(appended.first->Val, val);
	ASSERT_EQ(++appended.first, appended.second);
}

TEST(IntervalTree, AppendTwoIntersectsSameVal_FoundExactOne2)
{
	IntervalTree<char> testMap;
	Interval inter1{ 10, 20 };
	Interval inter2{ 5, 45 };
	Interval result{ 5, 45 };
	char val = 'c';
	testMap.insert_or_assign(inter1, val);
	testMap.insert_or_assign(inter2, val);

	auto appended = testMap.find(result, val);
	ASSERT_NE(appended.first, testMap.end());
	ASSERT_NE(appended.first, appended.second);
	ASSERT_EQ(appended.first->Inter.Low, result.Low);
	ASSERT_EQ(appended.first->Inter.High, result.High);
	ASSERT_EQ(appended.first->Val, val);
	ASSERT_EQ(testMap.size(), 1);
}

TEST(IntervalTree, AppendTwoIntersectsSameVal_FoundExactOne3)
{
	IntervalTree<char> testMap;
	Interval inter1{ 10, 20 };
	Interval inter2{ 15, 25 };
	Interval result{ 10, 25 };
	char val = 'c';
	testMap.insert_or_assign(inter1, val);
	testMap.insert_or_assign(inter2, val);

	auto appended = testMap.find(result, val);
	ASSERT_NE(appended.first, testMap.end());
	ASSERT_NE(appended.first, appended.second);
	ASSERT_EQ(appended.first->Inter.Low, result.Low);
	ASSERT_EQ(appended.first->Inter.High, result.High);
	ASSERT_EQ(appended.first->Val, val);
	ASSERT_EQ(testMap.size(), 1);
}

TEST(IntervalTree, AppendTwoIntersectsSameVal_FoundExactOne4)
{
	IntervalTree<char> testMap;
	Interval inter1{ 10, 20 };
	Interval inter2{ 15, 18 };
	Interval result{ 10, 20 };
	char val = 'c';
	testMap.insert_or_assign(inter1, val);
	testMap.insert_or_assign(inter2, val);

	auto appended = testMap.find(result, val);
	ASSERT_NE(appended.first, testMap.end());
	ASSERT_NE(appended.first, appended.second);
	ASSERT_EQ(appended.first->Inter.Low, result.Low);
	ASSERT_EQ(appended.first->Inter.High, result.High);
	ASSERT_EQ(appended.first->Val, val);
	ASSERT_EQ(testMap.size(), 1);
}

TEST(IntervalTree, AppendTwoIntersectsDiffVal_FoundExactBoth1)
{
	IntervalTree<char> testMap;
	Interval inter1{ 45, 55 };
	Interval inter2{ 30, 50 };
	char val1 = 'c';
	char val2 = 'b';
	testMap.insert_or_assign(inter1, val1);
	testMap.insert_or_assign(inter2, val2);

	auto appended1 = testMap.find(inter1, val1);
	ASSERT_NE(appended1.first, testMap.end());
	ASSERT_NE(appended1.first, appended1.second);
	ASSERT_EQ(appended1.first->Inter.Low, inter1.Low);
	ASSERT_EQ(appended1.first->Inter.High, inter1.High);
	ASSERT_EQ(appended1.first->Val, val1);

	auto appended2 = testMap.find(inter2, val2);
	ASSERT_NE(appended2.first, testMap.end());
	ASSERT_NE(appended2.first, appended2.second);
	ASSERT_EQ(appended2.first->Inter.Low, inter2.Low);
	ASSERT_EQ(appended2.first->Inter.High, inter2.High);
	ASSERT_EQ(appended2.first->Val, val2);
	ASSERT_EQ(testMap.size(), 2);
}

TEST(IntervalTree, AppendTwoIntersectsDiffVal_FoundExactBoth2)
{
	IntervalTree<char> testMap;
	Interval inter1{ 30, 55 };
	Interval inter2{ 30, 55 };
	char val1 = 'c';
	char val2 = 'b';
	testMap.insert_or_assign(inter1, val1);
	testMap.insert_or_assign(inter2, val2);
	
	ASSERT_EQ(testMap.size(), 2);
	auto appended1 = testMap.find(inter1, val1);
	ASSERT_NE(appended1.first, testMap.end());
	ASSERT_NE(appended1.first, appended1.second);
	ASSERT_EQ(appended1.first->Inter.Low, inter1.Low);
	ASSERT_EQ(appended1.first->Inter.High, inter1.High);
	ASSERT_EQ(appended1.first->Val, val1);

	auto appended2 = testMap.find(inter2, val2);
	ASSERT_NE(appended2.first, testMap.end());
	ASSERT_NE(appended2.first, appended2.second);
	ASSERT_EQ(appended2.first->Inter.Low, inter2.Low);
	ASSERT_EQ(appended2.first->Inter.High, inter2.High);
	ASSERT_EQ(appended2.first->Val, val2);
}

TEST(IntervalTree, AppendThreeIntersectsDiffVal_FoundExactThree1)
{
	IntervalTree<char> testMap;
	Interval inter1{ 45, 55 };
	Interval inter2{ 30, 40 };
	Interval inter3{ 35, 50 };
	char val1 = 'c';
	char val2 = 'b';
	char val3 = 'a';
	testMap.insert_or_assign(inter1, val1);
	testMap.insert_or_assign(inter2, val2);
	testMap.insert_or_assign(inter3, val3);

	auto appended1 = testMap.find(inter1, val1);
	ASSERT_NE(appended1.first, testMap.end());
	ASSERT_NE(appended1.first, appended1.second);
	ASSERT_EQ(appended1.first->Inter.Low, inter1.Low);
	ASSERT_EQ(appended1.first->Inter.High, inter1.High);
	ASSERT_EQ(appended1.first->Val, val1);

	auto appended2 = testMap.find(inter2, val2);
	ASSERT_NE(appended2.first, testMap.end());
	ASSERT_NE(appended2.first, appended2.second);
	ASSERT_EQ(appended2.first->Inter.Low, inter2.Low);
	ASSERT_EQ(appended2.first->Inter.High, inter2.High);
	ASSERT_EQ(appended2.first->Val, val2);

	auto appended3 = testMap.find(inter3, val3);
	ASSERT_NE(appended3.first, testMap.end());
	ASSERT_NE(appended3.first, appended3.second);
	ASSERT_EQ(appended3.first->Inter.Low, inter3.Low);
	ASSERT_EQ(appended3.first->Inter.High, inter3.High);
	ASSERT_EQ(appended3.first->Val, val3);
	ASSERT_EQ(testMap.size(), 3);
}

TEST(IntervalTree, AppendThreeIntersectsDiffVal_FoundExactThree2)
{
	IntervalTree<char> testMap;
	Interval inter1{ 45, 55 };
	Interval inter2{ 30, 40 };
	Interval inter3{ 25, 60 };
	char val1 = 'c';
	char val2 = 'b';
	char val3 = 'a';
	testMap.insert_or_assign(inter1, val1);
	testMap.insert_or_assign(inter2, val2);
	testMap.insert_or_assign(inter3, val3);

	auto appended1 = testMap.find(inter1, val1);
	ASSERT_NE(appended1.first, testMap.end());
	ASSERT_NE(appended1.first, appended1.second);
	ASSERT_EQ(appended1.first->Inter.Low, inter1.Low);
	ASSERT_EQ(appended1.first->Inter.High, inter1.High);
	ASSERT_EQ(appended1.first->Val, val1);

	auto appended2 = testMap.find(inter2, val2);
	ASSERT_NE(appended2.first, testMap.end());
	ASSERT_NE(appended2.first, appended2.second);
	ASSERT_EQ(appended2.first->Inter.Low, inter2.Low);
	ASSERT_EQ(appended2.first->Inter.High, inter2.High);
	ASSERT_EQ(appended2.first->Val, val2);

	auto appended3 = testMap.find(inter3, val3);
	ASSERT_NE(appended3.first, testMap.end());
	ASSERT_NE(appended3.first, appended3.second);
	ASSERT_EQ(appended3.first->Inter.Low, inter3.Low);
	ASSERT_EQ(appended3.first->Inter.High, inter3.High);
	ASSERT_EQ(appended3.first->Val, val3);
	ASSERT_EQ(testMap.size(), 3);
}

TEST(IntervalTree, AppendThreeIntersectsOneVal_FoundExactOne1)
{
	IntervalTree<char> testMap;
	Interval inter1{ 45, 55 };
	Interval inter2{ 30, 40 };
	Interval inter3{ 35, 50 };
	Interval result{ 30, 55 };
	char val = 'c';
	testMap.insert_or_assign(inter1, val);
	testMap.insert_or_assign(inter2, val);
	testMap.insert_or_assign(inter3, val);

	auto appended1 = testMap.find(result, val);
	ASSERT_NE(appended1.first, testMap.end());
	ASSERT_NE(appended1.first, appended1.second);
	ASSERT_EQ(appended1.first->Inter.Low, result.Low);
	ASSERT_EQ(appended1.first->Inter.High, result.High);
	ASSERT_EQ(appended1.first->Val, val);
	ASSERT_EQ(testMap.size(), 1);
}

TEST(IntervalTree, AppendThreeIntersectsOneVal_FoundExactOne2)
{
	IntervalTree<char> testMap;
	Interval inter1{ 45, 55 };
	Interval inter2{ 30, 40 };
	Interval inter3{ 25, 60 };
	Interval result{ 25, 60 };
	char val = 'c';
	testMap.insert_or_assign(inter1, val);
	testMap.insert_or_assign(inter2, val);
	testMap.insert_or_assign(inter3, val);

	auto appended1 = testMap.find(result, val);
	ASSERT_NE(appended1.first, testMap.end());
	ASSERT_NE(appended1.first, appended1.second);
	ASSERT_EQ(appended1.first->Inter.Low, result.Low);
	ASSERT_EQ(appended1.first->Inter.High, result.High);
	ASSERT_EQ(appended1.first->Val, val);
	ASSERT_EQ(testMap.size(), 1);
}

TEST(IntervalTree, AppendThreeNoIntersects_ReadThree)
{
	IntervalTree<char> testMap;
	Interval inter1{ 45, 55 };
	Interval inter2{ 35, 40 };
	Interval inter3{ 25, 30 };
	Interval result{ 20, 80 };
	char val = 'c';
	testMap.insert_or_assign(inter1, val);
	testMap.insert_or_assign(inter2, val);
	testMap.insert_or_assign(inter3, val);
	
	ASSERT_EQ(testMap.size(), 3);
	
	auto appended1 = testMap.find(result, val);
	ASSERT_NE(appended1.first, testMap.end());
	ASSERT_NE(appended1.first, appended1.second);
	ASSERT_EQ(appended1.first->Inter.Low, inter3.Low);
	ASSERT_EQ(appended1.first->Inter.High, inter3.High);
	ASSERT_EQ(appended1.first->Val, val);

	appended1.first++;
	ASSERT_NE(appended1.first, testMap.end());
	ASSERT_NE(appended1.first, appended1.second);
	ASSERT_EQ(appended1.first->Inter.Low, inter2.Low);
	ASSERT_EQ(appended1.first->Inter.High, inter2.High);
	ASSERT_EQ(appended1.first->Val, val);

	appended1.first++;
	ASSERT_NE(appended1.first, testMap.end());
	ASSERT_NE(appended1.first, appended1.second);
	ASSERT_EQ(appended1.first->Inter.Low, inter1.Low);
	ASSERT_EQ(appended1.first->Inter.High, inter1.High);
	ASSERT_EQ(appended1.first->Val, val);
}

TEST(IntervalTree, MultipleIntersects_ReadOne)
{
	IntervalTree<char> testMap;

	auto newZone = 'z';
	for (size_t i = 0; i < 1000; ++i)
	{
		Interval newZoneInt{ i, i + 1 };
		testMap.insert_or_assign(newZoneInt, newZone);
	}
	for (size_t i = 250; i < 1250; ++i)
	{
		Interval newZoneInt{ i, i + 1 };
		testMap.insert_or_assign(newZoneInt, newZone);
	}
	for (size_t i = 500; i < 1500; ++i)
	{
		Interval newZoneInt{ i, i + 1 };
		testMap.insert_or_assign(newZoneInt, newZone);
	}
	for (size_t i = 750; i < 1750; ++i)
	{
		Interval newZoneInt{ i, i + 1 };
		testMap.insert_or_assign(newZoneInt, newZone);
	}


	Interval result{ 100, 700 };
	
	ASSERT_EQ(testMap.size(), 1);
	
	auto appended1 = testMap.find(result, newZone);
	ASSERT_NE(appended1.first, testMap.end());
	ASSERT_NE(appended1.first, appended1.second);
	ASSERT_EQ(appended1.first->Inter.Low, 0);
	ASSERT_EQ(appended1.first->Inter.High, 1750);
	ASSERT_EQ(appended1.first->Val, newZone);
}

TEST(IntervalTree,TwoNoIntersectsOnePrevious_ReadTwo)
{
	IntervalTree<char> testMap;
	Interval first{ 20,30 };
	Interval second{ 50,60 };
	Interval toFirst{ 25,28 };
	auto val = 'c';
	testMap.insert_or_assign(first, val);
	testMap.insert_or_assign(second, val);
	testMap.insert_or_assign(toFirst, val);

	Interval result{ 10, 80 };
	
	ASSERT_EQ(testMap.size(), 2);
	
	auto appended1 = testMap.find(result, val);
	ASSERT_NE(appended1.first, testMap.end());
	ASSERT_NE(appended1.first, appended1.second);
	ASSERT_EQ(appended1.first->Inter.Low, first.Low);
	ASSERT_EQ(appended1.first->Inter.High, first.High);
	ASSERT_EQ(appended1.first->Val, val);

	++appended1.first;
	ASSERT_NE(appended1.first, testMap.end());
	ASSERT_NE(appended1.first, appended1.second);
	ASSERT_EQ(appended1.first->Inter.Low, second.Low);
	ASSERT_EQ(appended1.first->Inter.High, second.High);
	ASSERT_EQ(appended1.first->Val, val);
}