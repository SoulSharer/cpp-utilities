#include "gtest/gtest.h"
#include <future>
#include "thread_safe_queue.h"

using Neuro::Utilities::ThreadSafeQueue;

TEST(NotificationQueue, CreateNoExcept)
{
	ASSERT_NO_THROW(ThreadSafeQueue<int>{});
}

TEST(NotificationQueue, PushOnePopOne)
{
	auto queue = ThreadSafeQueue<int>{};
	queue.push(1);
	ASSERT_EQ(*queue.wait_try_pop(), 1);
}

TEST(NotificationQueue, PushTenPopTen)
{
	auto queue = ThreadSafeQueue<int>{};
	for (auto i = 0; i < 10; ++i)
	{
		queue.push(i);
	}
	for (auto i = 0; i < 10; ++i)
	{
		ASSERT_EQ(*queue.wait_try_pop(), i);
	}
}